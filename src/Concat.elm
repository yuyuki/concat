module Concat exposing (..)


when : Bool -> List a -> List a
when cond v =
    if cond then
        v

    else
        []


when1 : Bool -> a -> List a
when1 cond v =
    if cond then
        [ v ]

    else
        []


maybe : Maybe a -> (a -> List b) -> List b
maybe m f =
    case m of
        Just v ->
            f v

        Nothing ->
            []


maybe1 : Maybe a -> (a -> b) -> List b
maybe1 m f =
    case m of
        Just v ->
            [ f v ]

        Nothing ->
            []
